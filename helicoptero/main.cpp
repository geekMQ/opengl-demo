#include <windows.h>
#include <GL/glut.h>

GLint spinX, spinY; //Para rotar la escena
GLfloat Rotacion = 0;
GLfloat zoom=1;
GLfloat miracontrol =0;
GLfloat bala=-20;
GLfloat trigger=0;




void piso(){
GLfloat Largo=100;
glColor4ub (0,112,0,0);

glBegin(GL_QUADS);
glVertex3f(-Largo,-100,-Largo);
glVertex3f( Largo,-100,-Largo);
glVertex3f( Largo,-100, Largo);
glVertex3f(-Largo,-100, Largo);
glEnd();
//dibujar el eje y
        glBegin(GL_LINES);
        glColor4ub(203, 244, 66,0);
        glVertex3f(0,-1000,0);glVertex3f(0,1000,0);

        glEnd();
        glBegin(GL_LINES);
        glColor4ub(255,0, 0,0);
        glVertex3f(-1000,0,0);glVertex3f(1000,0,0);
        glEnd();
        glBegin(GL_LINES);
        glColor4ub(32, 178, 58,0);
        glVertex3f(0,0,-1000);glVertex3f(0,0,1000);
        glEnd();

}

void dibujarBaseHeliceSuperior(){
    glPushMatrix();
    GLUquadricObj * Objeto;
    Objeto = gluNewQuadric();
    gluQuadricDrawStyle(Objeto,GLU_FILL);
    glTranslatef(0,21,0);
    glRotatef(90,-1,0,0);
    glColor4ub(198,0,0,0);
    gluCylinder(Objeto,5,5,12,10,1);
    glPopMatrix();
    gluDeleteQuadric(Objeto);
}

void dibujarBaseHeliceTrasera(){
    glPushMatrix();
    GLUquadricObj * Objeto2;
    Objeto2 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto2,GLU_FILL);
    glTranslatef(80,0,0);

    glColor4ub(198,0,0,0);
    gluCylinder(Objeto2,2,2,13,10,1);
    glPopMatrix();
    gluDeleteQuadric(Objeto2);
}

void Helicoptero (void) {
     glColor4ub(0, 0,255, 0);   //Rojo
     glPushMatrix();
       //glTranslatef(50, 0, 0);
       //glRotatef(Rotacion, 0, -1, 0); //Para rotar la esfera
     glRotatef(90, 0, 1, 0);
     glScalef(0.7,0.7,1);
     glutWireSphere(30, 30, 10);
     glPopMatrix();

     //base superior helice
    glPushMatrix();
    GLUquadricObj * Objeto;
    Objeto = gluNewQuadric();
    gluQuadricDrawStyle(Objeto,GLU_FILL);
    glTranslatef(0,21,0);
    glRotatef(90,-1,0,0);
    glColor4ub(198,0,0,0);
    gluCylinder(Objeto,4,4,12,10,1);
    glPopMatrix();
    gluDeleteQuadric(Objeto);

    //base inferior helice
    glPushMatrix();
    GLUquadricObj * Objeto2;
    Objeto2 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto2,GLU_FILL);
    glTranslatef(80,0,0);

    glColor4ub(198,0,0,0);
    gluCylinder(Objeto2,2,2,13,10,1);
    glPopMatrix();
    gluDeleteQuadric(Objeto2);

    //cola helicoptero
    glPushMatrix();
    GLUquadricObj * Objeto3;
    Objeto3 = gluNewQuadric();

    gluQuadricDrawStyle(Objeto3,GLU_FILL);
    glTranslatef(80,0,0);
    glRotatef(90,0,1,0);
    glRotatef(45,-1,0,0);
    glColor4ub(0,0,255,0);
    glScalef(0.3,1,1);
    gluCylinder(Objeto3,5,3,25,10,1);
    gluDeleteQuadric(Objeto3);
    glPopMatrix();

    glPushMatrix();
    GLUquadricObj * Objeto10;
    Objeto10 = gluNewQuadric();

    gluQuadricDrawStyle(Objeto10,GLU_FILL);
    glTranslatef(80,0,0);
    glRotatef(90,0,1,0);
    glRotatef(-45,-1,0,0);
    glColor4ub(0,0,255,0);
    glScalef(0.3,1,1);
    gluCylinder(Objeto10,5,3,25,10,1);
    gluDeleteQuadric(Objeto10);
    glPopMatrix();
//-----------------------patas izquierdas------------------------
    //patas helicoptero
    glPushMatrix();
    GLUquadricObj * Objeto4;
    Objeto4 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto4,GLU_FILL);
    glTranslatef(-20,-35,20);
    glRotatef(90,0,1,0);
    glColor4ub(166, 170, 178,0);
    gluCylinder(Objeto4,3,3,40,10,1);
    gluDeleteQuadric(Objeto4);
    glPopMatrix();
    //esfera pata iz
     glPushMatrix();
     glColor4ub(198,0,0,0);   //Rojo
     glRotatef(90, 0, 1, 0);
     //glScalef(0.7,0.7,1);
     glTranslatef(-20,-35,20);
     glutSolidSphere(3, 20, 20);
     glPopMatrix();

     glPushMatrix();
     glColor4ub(198,0,0,0);   //Rojo
     glRotatef(90, 0, 1, 0);
     //glScalef(0.7,0.7,1);
     glTranslatef(-20,-35,-20);
     glutSolidSphere(3, 20, 20);
     glPopMatrix();
//sujetadores

    glPushMatrix();
    GLUquadricObj * Objeto5;
    Objeto5 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto5,GLU_FILL);
    glTranslatef(-10,-17,5);
    glRotatef(50,1, 0, 0);
    //glRotatef(90,0,1,0);
    glColor4ub(166, 170, 178,0);
    gluCylinder(Objeto5,2,2,25,10,1);
    gluDeleteQuadric(Objeto5);
    glPopMatrix();


    glPushMatrix();
    GLUquadricObj * Objeto6;
    Objeto6 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto6,GLU_FILL);
    glTranslatef(7,-17,5);
    glRotatef(50,1, 0, 0);
    //glRotatef(90,0,1,0);
    glColor4ub(166, 170, 178,0);
    gluCylinder(Objeto6,2,2,25,10,1);
    gluDeleteQuadric(Objeto6);
    glPopMatrix();
//-----------------------patas derechas------------------------
    glPushMatrix();
    GLUquadricObj * Objeto7;
    Objeto7 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto7,GLU_FILL);
    glTranslatef(-20,-35,-20);
    glRotatef(90,0,1,0);
    glColor4ub(166, 170, 178,0);
    gluCylinder(Objeto7,3,3,40,10,1);
    gluDeleteQuadric(Objeto7);
    glPopMatrix();

        //esfera pata iz
     glPushMatrix();
     glColor4ub(198,0,0,0);   //Rojo
     glRotatef(90, 0, 1, 0);
     //glScalef(0.7,0.7,1);
     glTranslatef(20,-35,20);
     glutSolidSphere(3, 20, 20);
     glPopMatrix();

     glPushMatrix();
     glColor4ub(198,0,0,0);   //Rojo
     glRotatef(90, 0, 1, 0);
     //glScalef(0.7,0.7,1);
     glTranslatef(20,-35,-20);
     glutSolidSphere(3, 20, 20);
     glPopMatrix();

    glPushMatrix();
    GLUquadricObj * Objeto8;
    Objeto8 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto8,GLU_FILL);
    glTranslatef(-10,-35,-20);
    glRotatef(50,-1, 0, 0);
    //glRotatef(90,0,1,0);
    glColor4ub(166, 170, 178,0);
    gluCylinder(Objeto8,2,2,25,10,1);
    gluDeleteQuadric(Objeto8);
    glPopMatrix();

    glPushMatrix();
    GLUquadricObj * Objeto9;
    Objeto9 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto9,GLU_FILL);
    glTranslatef(6,-35,-20);
    glRotatef(50,-1, 0, 0);
    //glRotatef(90,0,1,0);
    glColor4ub(166, 170, 178,0);
    gluCylinder(Objeto9,2,2,25,10,1);
    gluDeleteQuadric(Objeto9);
    glPopMatrix();

    //CREAR LASER DE EL HELICOPTEERO
    if(miracontrol == 1){
        glBegin(GL_LINES);
        glColor4ub(255,0,0,0);
        glVertex3f(-2000,-35,-20);glVertex3f(-20,-35,-20);
        glVertex3f(-2000,-35,20);glVertex3f(-20,-35,20);
        glEnd();
    }

    if(trigger==1){
            bala-=2;
    if(bala < -90){
        bala=-20;
        trigger=0;
    }
    //--- dibujar bala de la pata iz
     glPushMatrix();
     glColor4ub(198,0,0,0);   //Rojo
     glRotatef(90, 0, 1, 0);
     //glScalef(0.7,0.7,1);
     glTranslatef(-20,-35,bala);
     glutSolidSphere(3, 20, 20);
     glPopMatrix();

     // dibujar bala de la pata derecha

     glPushMatrix();
     glColor4ub(198,0,0,0);   //Rojo
     glRotatef(90, 0, 1, 0);
     //glScalef(0.7,0.7,1);
     glTranslatef(20,-35,bala);
     glutSolidSphere(3, 20, 20);
     glPopMatrix();
    }
}


void DibujarHeliceTracera (void) {
     glPushMatrix();
     glTranslatef(80,0,10);
     glRotatef(Rotacion, 0, 0, 1);
     glScalef(5,50,1);//parametros enteros si termina en d, f permite flotantes
     glutSolidCube (1.0);
     glPopMatrix();

     glPushMatrix();
     glTranslatef(80,0,10);
     glRotatef(Rotacion, 0, 0, 1);
     glScalef(50,5,1);//parametros enteros si termina en d, f permite flotantes
     glutSolidCube (1.0);
     glPopMatrix();

}

void DibujarHelice (void) {
     glColor4ub(0, 255, 0, 0);   //Verd

     glPushMatrix();
     glTranslatef(0,30,0);
     glRotatef(Rotacion, 0, -1, 0);
     glScalef(80,1,5);//parametros enteros si termina en d, f permite flotantes
     glutSolidCube (1.0);
     glPopMatrix();

     glPushMatrix();
     glTranslatef(0,30,0);
     glRotatef(Rotacion, 0, -1, 0);
     glScalef(5,1,80);//parametros enteros si termina en d, f permite flotantes
     glutSolidCube (1.0);
     glPopMatrix();
     DibujarHeliceTracera();
}

void dibujarCola(){
GLUquadricObj * Objeto;
Objeto = gluNewQuadric();
glPushMatrix();
gluQuadricDrawStyle(Objeto,GLU_FILL);
glTranslatef(28,0,0);
glRotatef(90,0,1,0);
glColor4ub(0,0,255,0);


gluCylinder(Objeto,6,2,56,10,1);
glPopMatrix();
gluDeleteQuadric(Objeto);
}



void display(void)
{
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   //Rota la escena
   glRotatef(spinX, 1, 0, 0);
   glRotatef(spinY, 0, 1, 0);

   glScalef(zoom,zoom,zoom);
   Helicoptero();
   DibujarHelice ();
   dibujarCola();
   piso();

   glutSwapBuffers();
}

void spinDisplay(void)
{
   Rotacion += 1;
   glutPostRedisplay(); //Vuelve a dibujar
}

void init(void) {
  glClearColor (0.0, 0.0, 0.0, 0.0); //Colores para iniciar la ventana (Fondo)
  glEnable(GL_DEPTH_TEST);glRotatef(Rotacion, 0, -1, 0);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glEnable(GL_NORMALIZE);
  glEnable(GL_COLOR_MATERIAL);
}

void reshape(int w, int h) {
   glViewport (0, 0, (GLsizei) w, (GLsizei) h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
     glColor4ub(137, 66, 244,0);   //Verde
     glPushMatrix();
     glScalef(50,5,1);//parametros enteros si termina en d, f permite flotantes
     glutSolidCube (2.0);
     glPopMatrix();
   glOrtho(-100.0, 100.0, -100.0, 100.0, -100.0, 100.0); //Izq, Der, Abajo, Arriba, Cerca, lejos
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   gluPerspective(45.0, (float)w / (float)h, 100.0, 100.0);
}

void handleKeypress(unsigned char key, int x, int y) {
  switch (key) {
    case 27: //Escape key
         exit(0);
    break;
        case 'z': //Escape key
         zoom -=0.1;
    break;
      case 'Z': //Escape key
         zoom +=0.1;
    break;
          case 'o': //Escape key
         spinX =90;
         spinY =0;
    break;
      case 'O': //Escape key
         spinX =0;
         spinY =90;
    break;
     case 'b': //Escape key
         if(miracontrol==0)
         miracontrol=1;
         else
            miracontrol=0;
            break;
     case 'c': //Escape key
         if(trigger==0)
            trigger=1;
         else
            trigger=0;
    break;
  }
}

void mouseMotion(int x, int y)
{
     spinX = y;
     spinY = x;
}

void mouse(int button, int state, int x, int y)
{
   switch (button) {
      case GLUT_LEFT_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(spinDisplay);
         break;

      case GLUT_RIGHT_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(NULL);
         break;

      default:
         break;
   }
}

int main(int argc, char** argv)
{
   glutInit(&argc, argv);   //Inicializa la librer�a GLUT y negocia una sesi�n
                            //con el sistema de ventanas
   glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH); //Activa el modo inicial de despliegue
                            //GLUT_DOUBLE doble buffer  GLUT_RGB = GLUT_RGBA
   glutInitWindowSize (600, 600); //Tama�o de la ventana 500 x 500
   glutInitWindowPosition (100, 100); //Posici�n de la ventana en la pantalla
   glutCreateWindow ("Esfera - Cubo"); //Crea la ventana y le pone la etiqueta

   init ();  //Ejecuta el m�todo "init"

   glutDisplayFunc(display);  //Ejecuta el m�todo "display"
   glutKeyboardFunc(handleKeypress);
   glutReshapeFunc(reshape); //Ejecuta el m�todo "reshape"
   glutMouseFunc(mouse); //Activa los controles del mouse
   glutMotionFunc(mouseMotion);
   glutMainLoop(); //Repite el main indefinidamente (�Animaci�n!)
   return 0;   /* ANSI C requires main to return int. */
}
