#include <windows.h>
#include <GL/glut.h>
#include "TGATextura.h"
#include <math.h>
#include <stdio.h>
char Direccion[50];
GLfloat Desplazamiento[50];

GLint maxPunt = 0;

GLfloat FLOOR_SIZE = 25.0f; //The length of each side of the floor

GLfloat Zoom = 1;

GLint Rot = 0;
GLfloat Rotacion = 0;
GLfloat RotacionHelice = 0;
GLfloat Rotacion2 = 0;
GLint contador = 0;
GLint prenderLuz = 0;
GLfloat Inclinacion = 30;
GLfloat tamaSphera = 0.3;
/*meter en spin displaycodigo de vernulli*/
    GLfloat x2 = 0;       //Posici�n en x
    GLfloat y2 = -7.6;   //Posici�n en y
    GLfloat z2 = 0;       //Posici�n en z
    GLfloat Angulo = 0;
    GLfloat r = 0;
/**------------------------------------------------------------------------------------------------------------------------*/
GLfloat miracontrol =0;
GLfloat bala=-20;
GLfloat trigger=0;

GLfloat rotate=-30;
GLfloat saltoBola=-20;
GLfloat cerca = 0;
GLint mTextura = 0;
GLint k = 0;
GLint limite = 300;
GLint planPos = 0;
GLint contadorLimite = 0;
GLint Meridianos = 0;


/**codigo para mover el helicoptero con plan de vuelo**/
GLint moverHelice=0;
GLint aux=0;
GLint permiso=0;
GLint moverVertical=0;
GLint moverLadoz=0;
GLint moverLadoX=0;
GLint ContadorBalas=0;

GLint planVueloIniciado=0;
GLint reversaVertical=0;
GLint reversaX=0;
GLint reversaZ=0;



GLfloat moverEnY = -100;
GLfloat moverEnX = -150;
GLfloat moverEnZ = 0;

GLint limiteCambioSeleccion=0;
GLint aumento=2;
GLfloat incremento=0.1;
///**  glulookat **/////

    GLdouble eyeX=0;
 	GLdouble eyeY=0;
 	GLdouble eyeZ=0;
 	GLdouble centerX=0;
 	GLdouble centerY=0;
 	GLdouble centerZ=0;
 	GLdouble upX=0;
 	GLdouble upY=0;
 	GLdouble upZ=0;


/**variables para la luz en 45 **/

GLfloat LIGHT_POS = 20.0f;
GLfloat BOX_HEIGHT = 7.0f;


GLint Lados = 30;

GLint isRightKeyPressed = 0;
GLint isLeftKeyPressed  = 0;
GLint isUpKeyPressed    = 0;
GLint isDownKeyPressed  = 0;

GLint ArrowColor;
GLint Cubo = 0;

GLfloat Luz = 0.5;
GLfloat Luz2 = 0.5;
GLfloat Transparencia = 200;

GLfloat aZh = 0;

GLfloat _angle = 0;

GLfloat spinX=0.0, spinY=0.0;

//Direcci�n
GLint dx = 0; GLint dy = -1; GLint dz = 0;

GLint Hx =0; GLint Hy = -1; GLint Hz =0;

//Posici�n
GLfloat rX = 0; GLfloat rY = 13.5; GLfloat rZ = 0;
GLfloat RX = 0; GLfloat RY = 7.5; GLfloat RZ = 0;


void drawBall(void) {
  glPushMatrix();
  glEnable(GL_TEXTURE_2D);
  glScalef(tamaSphera,tamaSphera,tamaSphera);
  glTranslatef(rotate,saltoBola, 20);
  glRotatef(90, 1, 0, 0);
  glRotatef(Rotacion2, 0, -1, 0);
    //Activa la textura
  glBindTexture(GL_TEXTURE_2D,texturas[3].ID);

  GLUquadricObj *sphere=NULL;
  sphere = gluNewQuadric();

  gluQuadricDrawStyle(sphere, GLU_FILL);
  gluQuadricTexture(sphere, TRUE);
  gluQuadricNormals(sphere, GLU_SMOOTH);
  glColor4ub(255, 255, 255, 0);
  gluSphere(sphere, 3+cerca, 24, 24);

  if (Meridianos == 1) {
      glColor4ub(25, 112, 112, 255);
      glutWireSphere(3.01+cerca, 24, 24);
  }
  glColor4ub(255, 255, 255, 0);
  glPopMatrix();

  gluDeleteQuadric(sphere);
  glDisable(GL_TEXTURE_2D);
}


void drawTeaPot(void){
glColor4ub(0,255,0,0);
glPushMatrix();
glTranslatef(7,-6,-8);
glutSolidTeapot(1);
glPopMatrix();
}
void drawPicture(){
glTranslatef(11,2,-6);
glRotatef(15,-1,0,0);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D,texturas[1].ID);

  glColor4ub(255, 255, 255, 0); //El color afecta la imagen
  glBegin(GL_QUADS);
    glTexCoord2f(0, 0); glVertex2f(-1, -1);  // Bottom Left Of The Texture and Quad
    glTexCoord2f(1, 0); glVertex2f( 1, -1);  // Bottom Right Of The Texture and Quad
    glTexCoord2f(1, 1); glVertex2f( 1,  1);  // Top Right Of The Texture and Quad
    glTexCoord2f(0, 1); glVertex2f(-1,  1);  // Top Left Of The Texture and Quad
  glEnd();
  glDisable(GL_TEXTURE_2D);


        glPushMatrix();
        glColor4ub(165, 127, 49,0);
        glTranslatef(0,0,-0.1);
             glScalef(1,1,0.01);
             glutSolidCube (3);
        glPopMatrix();

        glPushMatrix();
        glRotatef(15,1,0,0);
        glRotatef(15,1,0,0);
        glColor4ub(0, 0, 0,0);
        glTranslatef(0,-1,-0.60);
             glScalef(0.1,1,0.1);
             glutSolidCube (3);
        glPopMatrix();

}

void drawTable(void){

glPushMatrix();

        glTranslatef(10,-7,-5);
        GLfloat alto =15;
         GLUquadricObj * Objeto;
        glColor4ub(188, 136, 22,0);


         glScalef(0.2,0.2,0.2);
         glRotatef(90,-1,0,0);
        /**** dibujar 2 patas ****/
         glPushMatrix();
         glTranslatef(5,10,20);
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            //glRotatef(10, -1, 0, 0);
            gluCylinder(Objeto,3,2,alto,10,1);
         glPopMatrix();

         glPushMatrix();
         glTranslatef(5,10,19);
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            //glRotatef(10, -1, 0, 0);
            gluCylinder(Objeto,4,2,3,10,1);
         glPopMatrix();

        glPushMatrix();
         glTranslatef(5,10,18);
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            //glRotatef(10, -1, 0, 0);
            gluCylinder(Objeto,4,4,1,10,1);
         glPopMatrix();

         glPushMatrix();
         glTranslatef(5,10,15);
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            //glRotatef(10, -1, 0, 0);
            gluCylinder(Objeto,2,4,3,10,1);
         glPopMatrix();

       glPushMatrix();
        glScalef(0.8,0.8,0.8);
         glTranslatef(6.5,12.5,15);
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            //glRotatef(10, -1, 0, 0);
            gluSphere(Objeto,5,10,10);
         glPopMatrix();


        glPushMatrix();
         glTranslatef(5,10.0001,6);
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            //glRotatef(10, -1, 0, 0);
            gluCylinder(Objeto,2,3.50,4,10,1);
         glPopMatrix();


        glPushMatrix();
         glTranslatef(5,10.0001,3);
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            //glRotatef(10, -1, 0, 0);
            gluCylinder(Objeto,5,2,3,10,1);
         glPopMatrix();

        glPushMatrix();
         glTranslatef(5,10.0001,1);
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            //glRotatef(10, -1, 0, 0);
            gluCylinder(Objeto,10,10,2,20,1);
         glPopMatrix();

         glPushMatrix();
         glTranslatef(5,10.0001,3);
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            //glRotatef(10, -1, 0, 0);
            gluDisk(Objeto,0,10,20,32);
         glPopMatrix();
        /**** dibujar asiento ****/
          glPushMatrix();
          glColor4ub(165,97,13,0);
            glTranslatef(5,9,35);
            Objeto = gluNewQuadric();
            glScalef(1,1,0.1);
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            gluSphere(Objeto,20,50,10);
         glPopMatrix();
 glPopMatrix();
}
void texturaEjemplo(void) {
glPushMatrix();

  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D,texturas[0].ID);
  //glColor4ub(112, 112, 112, 0); //El color afecta la imagen
  glBegin(GL_QUADS);
    glTexCoord2f(0, 0); glVertex2f(-1, -1);  // Bottom Left Of The Texture and Quad
    glTexCoord2f(1, 0); glVertex2f( 1, -1);  // Bottom Right Of The Texture and Quad
    glTexCoord2f(1, 1); glVertex2f( 1,  1);  // Top Right Of The Texture and Quad
    glTexCoord2f(0, 1); glVertex2f(-1,  1);  // Top Left Of The Texture and Quad
  glEnd();
  glDisable(GL_TEXTURE_2D);
  glutSwapBuffers();

glPopMatrix();
}
void Helicoptero (void) {
     glColor4ub(0, 0,255, 0);   //Rojo
     glPushMatrix();
       //glTranslatef(50, 0, 0);
       //glRotatef(Rotacion, 0, -1, 0); //Para rotar la esfera
     glRotatef(90, 0, 1, 0);
     glScalef(0.7,0.7,1);
     glutWireSphere(30, 30, 10);
     glPopMatrix();

     //base superior helice
    glPushMatrix();
    GLUquadricObj * Objeto;
    Objeto = gluNewQuadric();
    gluQuadricDrawStyle(Objeto,GLU_FILL);
    glTranslatef(0,21,0);
    glRotatef(90,-1,0,0);
    glColor4ub(198,0,0,0);
    gluCylinder(Objeto,4,4,12,10,1);
    glPopMatrix();
    gluDeleteQuadric(Objeto);

    //base inferior helice
    glPushMatrix();
    GLUquadricObj * Objeto2;
    Objeto2 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto2,GLU_FILL);
    glTranslatef(80,0,0);

    glColor4ub(198,0,0,0);
    gluCylinder(Objeto2,2,2,13,10,1);
    glPopMatrix();
    gluDeleteQuadric(Objeto2);

    //cola helicoptero
    glPushMatrix();
    GLUquadricObj * Objeto3;
    Objeto3 = gluNewQuadric();

    gluQuadricDrawStyle(Objeto3,GLU_FILL);
    glTranslatef(80,0,0);
    glRotatef(90,0,1,0);
    glRotatef(45,-1,0,0);
    glColor4ub(0,0,255,0);
    glScalef(0.3,1,1);
    gluCylinder(Objeto3,5,3,25,10,1);
    gluDeleteQuadric(Objeto3);
    glPopMatrix();

    glPushMatrix();
    GLUquadricObj * Objeto10;
    Objeto10 = gluNewQuadric();

    gluQuadricDrawStyle(Objeto10,GLU_FILL);
    glTranslatef(80,0,0);
    glRotatef(90,0,1,0);
    glRotatef(-45,-1,0,0);
    glColor4ub(0,0,255,0);
    glScalef(0.3,1,1);
    gluCylinder(Objeto10,5,3,25,10,1);
    gluDeleteQuadric(Objeto10);
    glPopMatrix();
//-----------------------patas izquierdas------------------------
    //patas helicoptero
    glPushMatrix();
    GLUquadricObj * Objeto4;
    Objeto4 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto4,GLU_FILL);
    glTranslatef(-20,-35,20);
    glRotatef(90,0,1,0);
    glColor4ub(166, 170, 178,0);
    gluCylinder(Objeto4,3,3,40,10,1);
    gluDeleteQuadric(Objeto4);
    glPopMatrix();
    //esfera pata iz
     glPushMatrix();
     glColor4ub(198,0,0,0);   //Rojo
     glRotatef(90, 0, 1, 0);
     //glScalef(0.7,0.7,1);
     glTranslatef(-20,-35,20);
     glutSolidSphere(3, 20, 20);
     glPopMatrix();

     glPushMatrix();
     glColor4ub(198,0,0,0);   //Rojo
     glRotatef(90, 0, 1, 0);
     //glScalef(0.7,0.7,1);
     glTranslatef(-20,-35,-20);
     glutSolidSphere(3, 20, 20);
     glPopMatrix();
//sujetadores

    glPushMatrix();
    GLUquadricObj * Objeto5;
    Objeto5 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto5,GLU_FILL);
    glTranslatef(-10,-17,5);
    glRotatef(50,1, 0, 0);
    //glRotatef(90,0,1,0);
    glColor4ub(166, 170, 178,0);
    gluCylinder(Objeto5,2,2,25,10,1);
    gluDeleteQuadric(Objeto5);
    glPopMatrix();


    glPushMatrix();
    GLUquadricObj * Objeto6;
    Objeto6 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto6,GLU_FILL);
    glTranslatef(7,-17,5);
    glRotatef(50,1, 0, 0);
    //glRotatef(90,0,1,0);
    glColor4ub(166, 170, 178,0);
    gluCylinder(Objeto6,2,2,25,10,1);
    gluDeleteQuadric(Objeto6);
    glPopMatrix();
//-----------------------patas derechas------------------------
    glPushMatrix();
    GLUquadricObj * Objeto7;
    Objeto7 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto7,GLU_FILL);
    glTranslatef(-20,-35,-20);
    glRotatef(90,0,1,0);
    glColor4ub(166, 170, 178,0);
    gluCylinder(Objeto7,3,3,40,10,1);
    gluDeleteQuadric(Objeto7);
    glPopMatrix();

        //esfera pata iz
     glPushMatrix();
     glColor4ub(198,0,0,0);   //Rojo
     glRotatef(90, 0, 1, 0);
     //glScalef(0.7,0.7,1);
     glTranslatef(20,-35,20);
     glutSolidSphere(3, 20, 20);
     glPopMatrix();

     glPushMatrix();
     glColor4ub(198,0,0,0);   //Rojo
     glRotatef(90, 0, 1, 0);
     //glScalef(0.7,0.7,1);
     glTranslatef(20,-35,-20);
     glutSolidSphere(3, 20, 20);
     glPopMatrix();

    glPushMatrix();
    GLUquadricObj * Objeto8;
    Objeto8 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto8,GLU_FILL);
    glTranslatef(-10,-35,-20);
    glRotatef(50,-1, 0, 0);
    //glRotatef(90,0,1,0);
    glColor4ub(166, 170, 178,0);
    gluCylinder(Objeto8,2,2,25,10,1);
    gluDeleteQuadric(Objeto8);
    glPopMatrix();

    glPushMatrix();
    GLUquadricObj * Objeto9;
    Objeto9 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto9,GLU_FILL);
    glTranslatef(6,-35,-20);
    glRotatef(50,-1, 0, 0);
    //glRotatef(90,0,1,0);
    glColor4ub(166, 170, 178,0);
    gluCylinder(Objeto9,2,2,25,10,1);
    gluDeleteQuadric(Objeto9);
    glPopMatrix();

    //CREAR LASER DE EL HELICOPTEERO
    if(miracontrol == 1){
        glBegin(GL_LINES);
        glColor4ub(255,0,0,0);
        glVertex3f(-2000,-35,-20);glVertex3f(-20,-35,-20);
        glVertex3f(-2000,-35,20);glVertex3f(-20,-35,20);
        glEnd();
    }

    if(trigger==1){
                    bala-=2;
            if(bala < -90){
                bala=-20;
                trigger=0;
            }
            //--- dibujar bala de la pata iz
             glPushMatrix();
             glColor4ub(198,0,0,0);   //Rojo
             glRotatef(90, 0, 1, 0);
             //glScalef(0.7,0.7,1);
             glTranslatef(-20,-35,bala);
             glutSolidSphere(3, 20, 20);
             glPopMatrix();

             // dibujar bala de la pata derecha

             glPushMatrix();
             glColor4ub(198,0,0,0);   //Rojo
             glRotatef(90, 0, 1, 0);
             //glScalef(0.7,0.7,1);
             glTranslatef(20,-35,bala);
             glutSolidSphere(3, 20, 20);
             glPopMatrix();
    }
}
void DibujarHeliceTracera (void) {
     glPushMatrix();
     glTranslatef(80,0,10);
     glRotatef(RotacionHelice, 0, 0, 1);
     glScalef(5,50,1);//parametros enteros si termina en d, f permite flotantes
     glutSolidCube (1.0);
     glPopMatrix();

     glPushMatrix();
     glTranslatef(80,0,10);
     glRotatef(RotacionHelice, 0, 0, 1);
     glScalef(50,5,1);//parametros enteros si termina en d, f permite flotantes
     glutSolidCube (1.0);
     glPopMatrix();

}
void DibujarHelice (void) {
     glColor4ub(0, 255, 0, 0);   //Verd

     glPushMatrix();
     glTranslatef(0,30,0);
     glRotatef(RotacionHelice, 0, -1, 0);
     glScalef(80,1,5);//parametros enteros si termina en d, f permite flotantes
     glutSolidCube (1.0);
     glPopMatrix();

     glPushMatrix();
     glTranslatef(0,30,0);
     glRotatef(RotacionHelice, 0, -1, 0);
     glScalef(5,1,80);//parametros enteros si termina en d, f permite flotantes
     glutSolidCube (1.0);
     glPopMatrix();
     DibujarHeliceTracera();
}
void dibujarCola(){
GLUquadricObj * Objeto;
Objeto = gluNewQuadric();
glPushMatrix();
gluQuadricDrawStyle(Objeto,GLU_FILL);
glTranslatef(28,0,0);
glRotatef(90,0,1,0);
glColor4ub(0,0,255,0);


gluCylinder(Objeto,6,2,56,10,1);
glPopMatrix();
gluDeleteQuadric(Objeto);
}
void draw_helicoptero(){
    glPushMatrix();
    GLfloat scale = 0.05;

    glScalef(scale,scale,scale);
    glTranslatef(moverEnX,moverEnY,moverEnZ);

  GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
  GLfloat diffuseMaterial[] = { 1.0, 1.0, 1.0, 0.0 };
  GLfloat lmodel_ambient[] = { Luz, Luz, Luz, Luz };
  GLfloat lightColor[] = {2.0f, 2.0f, 3.0f, 0.0f}; //Amarillo
  GLfloat lightPos[] = {RX, RY, RZ, 1};
  glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
  glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
  glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseMaterial);
  //glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
  glMaterialf(GL_FRONT, GL_SHININESS, 65.0f);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

   GLfloat spot_direction[] = { Hx, Hy, Hz };
   glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 60.0);
   glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, spot_direction);
   glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, 2.0);

   glRotatef(90,0,1,0);

    Helicoptero();
    DibujarHelice ();
    dibujarCola();
    glPopMatrix();


   /* glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
        gluLookAt(0, 0, -1,
              moverEnX, moverEnY, moverEnZ,
              0, 1, 0
        );*/

}
/**------------------------------------------------------------------------------------------------------------------------*/
void draw_trompo (void) {
    glPushMatrix();
    GLfloat scale = 0.025;
    glScalef(scale,scale,scale);
        glTranslatef(400,-200,0);
        glRotatef(Rotacion, 0, -1, 0);
        glRotatef(Inclinacion,1,0,0);
        glTranslatef(x2,0,0);
            GLUquadricObj * Objeto;
            GLUquadricObj * Objeto1;
            GLUquadricObj * Objeto2;
            GLUquadricObj * Objeto3;
            GLUquadricObj * Objeto4;
            GLUquadricObj * Objeto5;
            GLUquadricObj * Objeto6;
            GLUquadricObj * Objeto7;
            GLUquadricObj * Objeto8;
            GLUquadricObj * Objeto9;
            GLUquadricObj* quadratic;


          glPushMatrix();
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            glTranslatef(0,0,0);
            glColor4ub(255, 255, 0,0);
            glRotatef(90,-1,0,0);
            gluCylinder(Objeto,8,8,5,20,1);
          glPopMatrix();

           glPushMatrix();
            quadratic = gluNewQuadric();
            glTranslatef(0,5,0);
            glRotatef(90,-1,0,0);
            glColor4ub(255, 0, 0,0);
            gluDisk(quadratic,0,9,10,32);
          glPopMatrix();
/********************************************/
        //tapa
         glPushMatrix();
            quadratic = gluNewQuadric();
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D,texturas[4].ID);
            gluQuadricTexture(quadratic, GL_TRUE);
            glTranslatef(0,0,0);
            glRotatef(90,-1,0,0);
            glColor4ub(255, 255, 0, 0); //Amarillo
            gluDisk(quadratic,0,25,20,32);
            glDisable(GL_TEXTURE_2D);
          glPopMatrix();



/****************************************/

         glPushMatrix();
            Objeto1 = gluNewQuadric();
            gluQuadricDrawStyle(Objeto1,GLU_FILL);
             glColor4ub(255, 255, 0,0);
            glTranslatef(0,-5,0);
            glRotatef(90,-1,0,0);
            gluCylinder(Objeto1,32,25,5,20,1);
         glPopMatrix();

          glPushMatrix();
            Objeto2 = gluNewQuadric();
            gluQuadricDrawStyle(Objeto2,GLU_FILL);
            glTranslatef(0,-10,0);
             glColor4ub(255, 0, 0,0);
            glRotatef(90,-1,0,0);
            gluCylinder(Objeto2,34,32,5,20,1);
         glPopMatrix();

           glPushMatrix();
            Objeto3 = gluNewQuadric();
            gluQuadricDrawStyle(Objeto3,GLU_FILL);
            glTranslatef(0,-15,0);
             glColor4ub(255, 255, 0,0);
            glRotatef(90,-1,0,0);
            gluCylinder(Objeto3,36,34,5,20,1);
         glPopMatrix();

           glPushMatrix();
            Objeto4 = gluNewQuadric();
            gluQuadricDrawStyle(Objeto4,GLU_FILL);
            glTranslatef(0,-20,0);
             glColor4ub(255, 0, 0,0);
            glRotatef(90,-1,0,0);
            gluCylinder(Objeto4,36,36,5,20,1);
          glPopMatrix();

           glPushMatrix();
            Objeto5 = gluNewQuadric();
            gluQuadricDrawStyle(Objeto5,GLU_FILL);
            glTranslatef(0,-25,0);
             glColor4ub(255, 255, 0,0);
            glRotatef(90,-1,0,0);
            gluCylinder(Objeto5,34,36,5,20,1);
          glPopMatrix();

        glPushMatrix();
            Objeto6 = gluNewQuadric();
            gluQuadricDrawStyle(Objeto6,GLU_FILL);
            glTranslatef(0,-30,0);
             glColor4ub(255, 0, 0,0);
            glRotatef(90,-1,0,0);
            gluCylinder(Objeto6,32,34,5,20,1);
          glPopMatrix();

          glPushMatrix();
            Objeto7 = gluNewQuadric();
            gluQuadricDrawStyle(Objeto7,GLU_FILL);
            glTranslatef(0,-75,0);
            glColor4ub(255, 255, 0,0);
            glRotatef(90,-1,0,0);
            gluCylinder(Objeto7,0,32,45,20,1);
          glPopMatrix();

        glColor4ub(42, 51, 49,0);
        glPushMatrix();
            Objeto8 = gluNewQuadric();
            gluQuadricDrawStyle(Objeto8,GLU_FILL);
            glTranslatef(0,-78,0);
            glRotatef(90,-1,0,0);
            gluCylinder(Objeto8,1,1,5,20,1);
          glPopMatrix();

          glPushMatrix();
            Objeto9 = gluNewQuadric();
            gluQuadricDrawStyle(Objeto9,GLU_FILL);
            glTranslatef(0,-80,0);

            glRotatef(90,-1,0,0);
            gluCylinder(Objeto9,0,2,5,20,1);
          glPopMatrix();
glPopMatrix();
}

void draw_copa_acostada(){

    glPushMatrix();
    GLfloat scale = 0.025;
    glScalef(scale,scale,scale);
    GLint scala = 0.1;
    glTranslatef(320,-240,350);
    glColor4ub(124, 124, 124,Transparencia);
    GLUquadricObj * quadriqCopa;
    glRotatef(90,-1,0,0);
    glRotatef(90,0,0,1);
    glRotatef(10,1,0,0);
             glPushMatrix();
                quadriqCopa = gluNewQuadric();
                gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
                //glTranslatef(0,0,0);
                glRotatef(90,-1,0,0);
                gluCylinder(quadriqCopa,30,20,20,20,1);
             glPopMatrix();

             glPushMatrix();
                quadriqCopa = gluNewQuadric();
                gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
                glTranslatef(0,-3,0);
                glRotatef(90,-1,0,0);
                gluCylinder(quadriqCopa,31,29,5,20,1);
             glPopMatrix();

             glPushMatrix();
                quadriqCopa = gluNewQuadric();
                gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
                glTranslatef(0,-5,0);
                glRotatef(90,-1,0,0);

                gluCylinder(quadriqCopa,32,30,5,20,1);
             glPopMatrix();

              glPushMatrix();
                quadriqCopa = gluNewQuadric();
                gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
                glTranslatef(0,-10,0);
                glRotatef(90,-1,0,0);

                gluCylinder(quadriqCopa,34,32,5,20,1);
             glPopMatrix();

               glPushMatrix();
                quadriqCopa = gluNewQuadric();
                gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
                glTranslatef(0,-15,0);
                glRotatef(90,-1,0,0);
                gluCylinder(quadriqCopa,36,34,5,20,1);
             glPopMatrix();

               glPushMatrix();
                quadriqCopa = gluNewQuadric();
                gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
                glTranslatef(0,-20,0);
                glRotatef(90,-1,0,0);
                gluCylinder(quadriqCopa,36,36,5,20,1);
              glPopMatrix();

               glPushMatrix();
                quadriqCopa = gluNewQuadric();
                gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
                glTranslatef(0,-25,0);
                glRotatef(90,-1,0,0);
                gluCylinder(quadriqCopa,34,36,5,20,1);
              glPopMatrix();

            glPushMatrix();
                quadriqCopa = gluNewQuadric();
                gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
                glTranslatef(0,-30,0);
                glRotatef(90,-1,0,0);
                gluCylinder(quadriqCopa,32,34,5,20,1);
              glPopMatrix();

              glPushMatrix();
                quadriqCopa = gluNewQuadric();
                gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
                glTranslatef(0,-75,0);
                glRotatef(90,-1,0,0);
                gluCylinder(quadriqCopa,5,32,45,20,1);
              glPopMatrix();

              /*base de la copa*/

                   //base superior helice
              glPushMatrix();
                quadriqCopa = gluNewQuadric();
                gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
                glTranslatef(0,-100,0);
                glRotatef(90,-1,0,0);
                gluCylinder(quadriqCopa,5,5,30,10,1);
              glPopMatrix();

              glPushMatrix();
                quadriqCopa = gluNewQuadric();
                gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
                glTranslatef(0,-110,0);
                glRotatef(90,-1,0,0);
                gluCylinder(quadriqCopa,20,5,10,10,1);
              glPopMatrix();

              glPushMatrix();
                quadriqCopa = gluNewQuadric();
                gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
                glTranslatef(0,-110,0);
                glRotatef(90,-1,0,0);
                gluCylinder(quadriqCopa,20,0,10,10,1);
              glPopMatrix();
                gluDeleteQuadric(quadriqCopa);

    glPopMatrix();
}

void draw_copa(){

glPushMatrix();

GLfloat scale = 0.025;
glScalef(scale,scale,scale);
GLint scala = 0.1;
glTranslatef(40,133,350);

glColor4ub(124, 124, 124,Transparencia);
GLUquadricObj * quadriqCopa;
GLUquadricObj * quadriqCopa1;
GLUquadricObj * quadriqCopa2;
GLUquadricObj * quadriqCopa3;

         glPushMatrix();
            quadriqCopa = gluNewQuadric();
            gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
            //glTranslatef(0,0,0);
            glRotatef(90,-1,0,0);
            gluCylinder(quadriqCopa,30,20,20,20,1);
         glPopMatrix();

         glPushMatrix();
            quadriqCopa = gluNewQuadric();
            gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
            glTranslatef(0,-3,0);
            glRotatef(90,-1,0,0);
            gluCylinder(quadriqCopa,31,29,5,20,1);
         glPopMatrix();

         glPushMatrix();
            quadriqCopa = gluNewQuadric();
            gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
            glTranslatef(0,-5,0);
            glRotatef(90,-1,0,0);
            gluCylinder(quadriqCopa,32,30,5.5,20,1);
         glPopMatrix();

          glPushMatrix();
            quadriqCopa = gluNewQuadric();
            gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
            glTranslatef(0,-10,0);
            glRotatef(90,-1,0,0);
            gluCylinder(quadriqCopa,34,32,5,20,1);
         glPopMatrix();

           glPushMatrix();
            quadriqCopa = gluNewQuadric();
            gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
            glTranslatef(0,-15,0);
            glRotatef(90,-1,0,0);
            gluCylinder(quadriqCopa,36,34,5,20,1);
         glPopMatrix();

           glPushMatrix();
            quadriqCopa = gluNewQuadric();
            gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
            glTranslatef(0,-20,0);
            glRotatef(90,-1,0,0);
            gluCylinder(quadriqCopa,36,36,5,20,1);
          glPopMatrix();

           glPushMatrix();
            quadriqCopa = gluNewQuadric();
            gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
            glTranslatef(0,-25,0);
            glRotatef(90,-1,0,0);
            gluCylinder(quadriqCopa,34,36,5,20,1);
          glPopMatrix();

        glPushMatrix();
            quadriqCopa = gluNewQuadric();
            gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
            glTranslatef(0,-30,0);
            glRotatef(90,-1,0,0);
            gluCylinder(quadriqCopa,32,34,5,20,1);
          glPopMatrix();

          glPushMatrix();
            quadriqCopa = gluNewQuadric();
            gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
            glTranslatef(0,-75,0);
            glRotatef(90,-1,0,0);
            gluCylinder(quadriqCopa,5,32,45,20,1);
          glPopMatrix();

          /*base de la copa*/

               //base superior helice
          glPushMatrix();
            quadriqCopa = gluNewQuadric();
            gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
            glTranslatef(0,-100,0);
            glRotatef(90,-1,0,0);
            gluCylinder(quadriqCopa,5,5,30,10,1);
          glPopMatrix();

          glPushMatrix();
            quadriqCopa = gluNewQuadric();
            gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
            glTranslatef(0,-110,0);
            glRotatef(90,-1,0,0);
            gluCylinder(quadriqCopa,20,5,10,10,1);
          glPopMatrix();

          glPushMatrix();
            quadriqCopa = gluNewQuadric();
            gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
            glTranslatef(0,-110,0);
            glRotatef(90,-1,0,0);
            gluCylinder(quadriqCopa,20,0,10,10,1);
          glPopMatrix();
            gluDeleteQuadric(quadriqCopa);
glPopMatrix();
}


void drawFloor(){
    /********texturas************/
    //glEnable(GL_TEXTURE_2D);
    //glBindTexture(GL_TEXTURE_2D,texturas[0].ID);
    /********************/
    GLfloat Lado = 12;
    GLfloat Brick = Lado/8;
    GLint Cuadros = 18;

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,texturas[0].ID);
    glPushMatrix();
    glTranslatef(-Brick, 0, 0);
    glBegin(GL_QUADS);
    for(int i=0;i<Cuadros;i++){
       for(int j=0;j<Cuadros;j++){
            if((i+j)%2==0){
                glColor4ub(25, 25, 112,Transparencia);
            }else{
                glColor4ub(112, 112, 112,Transparencia);
            }
                glTexCoord2f(0, 0); glVertex3f(-Lado+(i*Brick), 0, -Lado+(j*Brick));
                glTexCoord2f(1, 0); glVertex3f(-(Lado-Brick)+(i*Brick), 0, -Lado+(j*Brick));
                glTexCoord2f(1, 1); glVertex3f(-(Lado-Brick)+(i*Brick), 0, -(Lado-Brick)+(j*Brick));
                glTexCoord2f(0, 1); glVertex3f(-Lado+(i*Brick), 0, -(Lado-Brick)+(j*Brick));
        }
    }
  glEnd();
 // glDisable(GL_TEXTURE_2D);
 // glutSwapBuffers();
  glPopMatrix();
  glDisable(GL_TEXTURE_2D);
}

void drawUmbrella_esqueleto (void) {

   GLint Lados = 30;
   GLUquadricObj *qobj; //Define el objeto
   qobj = gluNewQuadric();
   gluQuadricDrawStyle(qobj, GLU_LINE);
    GLfloat ambient[]   = { 0.20, 0.05, 0.05, 1.0 };
    GLfloat diffuse[]   = { 0.89, 0.64, 0.14, 1.0 };
    GLfloat specular[]  = { 0.00, 0.00, 0.00, 1.0 };
    GLfloat emission[]  = { 0.00, 0.00, 0.00, 1.0 };
    GLfloat shininess[] = { 128.0 };

    glMaterialfv( GL_FRONT, GL_AMBIENT,   ambient );
    glMaterialfv( GL_FRONT, GL_DIFFUSE,   diffuse );
    glMaterialfv( GL_FRONT, GL_SPECULAR,  specular );
    glMaterialfv( GL_FRONT, GL_EMISSION,  emission );
    glMaterialfv( GL_FRONT, GL_SHININESS, shininess );

   glPushMatrix();
     glTranslatef(0, -3.730 ,0);
     glRotatef(45, 1, 0, 0);
     glColor4ub(0, 0, 0, 0); //Capa roja
     gluCylinder(qobj, 0, 3.9, 1, 11, 1);
   glPopMatrix();
   gluDeleteQuadric(qobj);


    gluQuadricDrawStyle(qobj, GLU_LINE);
    glMaterialfv( GL_FRONT, GL_AMBIENT,   ambient );
    glMaterialfv( GL_FRONT, GL_DIFFUSE,   diffuse );
    glMaterialfv( GL_FRONT, GL_SPECULAR,  specular );
    glMaterialfv( GL_FRONT, GL_EMISSION,  emission );
    glMaterialfv( GL_FRONT, GL_SHININESS, shininess );

   glPushMatrix();
     glTranslatef(-0.001, -4.2 ,0.50);
     glRotatef(45, 1, 0, 0);
     glColor4ub(0, 0, 0, 0); //Capa roja
     gluCylinder(qobj, 2, 0, 1, 6, 1);
   glPopMatrix();
   gluDeleteQuadric(qobj);

}

void drawUmbrella (void) {

   GLint Lados = 30;
   GLUquadricObj *qobj; //Define el objeto
   qobj = gluNewQuadric();
   gluQuadricDrawStyle(qobj, GLU_FILL);
    GLfloat ambient[]   = { 0.20, 0.05, 0.05, 1.0 };
    GLfloat diffuse[]   = { 0.89, 0.64, 0.14, 1.0 };
    GLfloat specular[]  = { 0.00, 0.00, 0.00, 1.0 };
    GLfloat emission[]  = { 0.00, 0.00, 0.00, 1.0 };
    GLfloat shininess[] = { 128.0 };

    glMaterialfv( GL_FRONT, GL_AMBIENT,   ambient );
    glMaterialfv( GL_FRONT, GL_DIFFUSE,   diffuse );
    glMaterialfv( GL_FRONT, GL_SPECULAR,  specular );
    glMaterialfv( GL_FRONT, GL_EMISSION,  emission );
    glMaterialfv( GL_FRONT, GL_SHININESS, shininess );

   glPushMatrix();
     glTranslatef(0, -3.7 ,0);
     glRotatef(45, 1, 0, 0);
     glColor4ub(255, 0, 0, 0); //Capa roja
     gluCylinder(qobj, 0, 3.9, 1, 11, 1);
     glColor4ub(100, 100, 100, 0); //Palo gris
     gluCylinder(qobj, 0.05, 0.05, 4.7, 10, 1);
   glPopMatrix();
   gluDeleteQuadric(qobj);
   drawUmbrella_esqueleto();

}

void drawChair(){

glPushMatrix();

        glTranslatef(0,-7,0);
        GLfloat alto =35;
         GLUquadricObj * Objeto;
        glColor4ub(188, 136, 22,0);

         glTranslatef(0,0,10);
         glScalef(0.2,0.2,0.2);
         glRotatef(90,-1,0,0);
        /**** dibujar 2 patas ****/
         glPushMatrix();
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            glRotatef(10, -1, 0, 0);
            gluCylinder(Objeto,1,1,alto,10,1);
         glPopMatrix();


          glPushMatrix();
            glTranslatef(10,0,0);
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            glRotatef(10, -1, 0, 0);
            gluCylinder(Objeto,1,1,alto,10,1);
         glPopMatrix();
        /**** dibujar 2 patas ****/
        glPushMatrix();
         glTranslatef(0,15,0);
         glPushMatrix();
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            glRotatef(8, 1, 0, 0);
            gluCylinder(Objeto,1,1,alto,10,1);
         glPopMatrix();


          glPushMatrix();
            glTranslatef(8,0,0);
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            glRotatef(10, 1, 0, 0);
            gluCylinder(Objeto,1,1,alto,10,1);
         glPopMatrix();

        glPopMatrix();

        /**** dibujar asiento ****/
          glPushMatrix();
          glColor4ub(165,97,13,0);
            glTranslatef(5,9,35);
            Objeto = gluNewQuadric();
            glScalef(1,1,0.3);
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            gluSphere(Objeto,10,10,10);
         glPopMatrix();
 glPopMatrix();
}

void dibujarLampara(){
    glPushMatrix();
    glColor4ub(197, 66, 244, 100);
         GLUquadricObj * Objeto;
         glTranslatef(0, rY+1.5, 0);
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            glRotatef(90, 1, 0, 0);
            gluCylinder(Objeto,0,1,2,20,1);
         glPopMatrix();

      glPushMatrix();
      glColor4ub(255, 255, 255, 100);

         glTranslatef(0, rY+10, 0);
            Objeto = gluNewQuadric();
            gluQuadricDrawStyle(Objeto,GLU_FILL);
            glRotatef(90, 1, 0, 0);
            glScalef( 0.1, 0.1,  0.1);
            gluCylinder(Objeto,1,1,100,20,10);
         glPopMatrix();

}

void manejarEventoLuz(){

if(prenderLuz==1){

GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
  GLfloat diffuseMaterial[] = { 1.0, 1.0, 1.0, 0.0 };
  GLfloat lmodel_ambient[] = { Luz, Luz, Luz, Luz };
  GLfloat lightColor[] = {1.0f, 1.0f, 0.0f, 0.0f}; //Amarillo
  GLfloat lightPos[] = {rX, rY, rZ, 1};
  glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
  glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
  glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseMaterial);
  //glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
  glMaterialf(GL_FRONT, GL_SHININESS, 65.0f);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

   GLfloat spot_direction[] = { dx, dy, dz };
   glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 45.0);
   glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, spot_direction);
   glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, 2.0);
   //printf("dx=%d dy=%d dz=%d \n", dx, dy, dz);

}

}

void manejarEventoLuzHelicoptero(){



GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
  GLfloat diffuseMaterial[] = { 1.0, 1.0, 1.0, 0.0 };
  GLfloat lmodel_ambient[] = { Luz, Luz, Luz, Luz };
  GLfloat lightColor[] = {1.0f, 1.0f, 0.0f, 0.0f}; //Amarillo
  GLfloat lightPos[] = {rX, rY, rZ, 1};
  glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
  glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
  glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseMaterial);
  //glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
  glMaterialf(GL_FRONT, GL_SHININESS, 65.0f);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

   GLfloat spot_direction[] = { Hx, Hy, Hz };
   glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 45.0);
   glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, spot_direction);
   glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, 2.0);
   //printf("dx=%d dy=%d dz=%d \n", dx, dy, dz);

}
void drawScene() {

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);


  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glTranslatef(0, 0, -40);
       /* gluLookAt(0, 0, -1,
              moverEnX, moverEnY, moverEnZ,
              0, 1, 0
        );*/
  glRotatef(30, 1, 0, 0);
  glScalef(Zoom, Zoom, Zoom);

  glRotatef(spinX, 1, 0, 0);
  glRotatef(spinY, 0, 1, 0);
  if (Rot == 1)
      glRotatef(Rotacion, 0, 1, 0);

  //Activa la textura

 glPushMatrix();
    glColor4ub(255, 255, 0, 1);
    glTranslatef(rX, rY, rZ);
    glutSolidSphere(0.3, 50, 50);
  glPopMatrix();
/***********************************************************/
    glPushMatrix();
      dibujarLampara();
      manejarEventoLuz();
    glPopMatrix();
/***********************************************************/


  glPushMatrix();
    glTranslatef(0, BOX_HEIGHT, 0);
    drawUmbrella();
    drawChair();

glEnable(GL_BLEND);
    draw_copa();
    draw_copa_acostada();

glDisable(GL_BLEND);
     drawBall();
    draw_trompo();
    draw_helicoptero();
    drawTable();
    drawTeaPot();
    drawPicture();
  glPopMatrix();

  glEnable(GL_STENCIL_TEST); //Enable using the stencil buffer
  glColorMask(0, 0, 0, 0); //Disable drawing colors to the screen
  glDisable(GL_DEPTH_TEST); //Disable depth testing
  glStencilFunc(GL_ALWAYS, 1, 1); //Make the stencil test always pass
  //Make pixels in the stencil buffer be set to 1 when the stencil test passes
  glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
  //Aqu� se dibuja el piso
  drawFloor();
  glColorMask(1, 1, 1, 1); //Enable drawing colors to the screen
  glEnable(GL_DEPTH_TEST); //Enable depth testing

  //Make the stencil test pass only when the pixel is 1 in the stencil buffer
  glStencilFunc(GL_EQUAL, 1, 1);
  glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP); //Make the stencil buffer not change

  //Draw the cube, reflected vertically, at all pixels where the stencil
  //buffer is 1
  glPushMatrix();
    glScalef(1, -1, 1);
    glTranslatef(0, BOX_HEIGHT, 0);
    //Aqu� se dibujan los objetos que se van a reflejar
    drawUmbrella();
    drawChair();
glEnable(GL_BLEND);
    draw_copa();
    draw_copa_acostada();
glDisable(GL_BLEND);
  //Activa la textura
    draw_trompo();
    draw_helicoptero();
    drawTeaPot();
    drawTable();
       drawBall();
  glPopMatrix();

  glDisable(GL_STENCIL_TEST); //Disable using the stencil buffer
  //Blend the floor onto the screen
  glEnable(GL_BLEND);
  //Aqu� se dibuja el piso que refleja
  drawFloor();

  glDisable(GL_BLEND);

  glutSwapBuffers();

}

void handleSpecialKeypress(int key, int x, int y) {

 switch (key) {
    case GLUT_KEY_LEFT:
         isLeftKeyPressed = 1;
         if (!isRightKeyPressed) {
             if(rX > -25)
                --rX;
         }
    break;

    case GLUT_KEY_RIGHT:
         isRightKeyPressed = 1;
         if (!isLeftKeyPressed) {
             if (rX < 25)
                ++rX;
         }
    break;

    case GLUT_KEY_UP:
         isUpKeyPressed = 1;
         if (!isDownKeyPressed) {
             if (rY < 25)
                ++rY;
         }
    break;

    case GLUT_KEY_DOWN:
         isDownKeyPressed = 1;
         if (!isUpKeyPressed) {
             if (rY > 0)
                --rY;
         }
    break;
 }
}

void handleSpecialKeyReleased(int key, int x, int y) {
 switch (key) {
 case GLUT_KEY_LEFT:
      isLeftKeyPressed = 0;
 break;

 case GLUT_KEY_RIGHT:
      isRightKeyPressed = 0;
 break;

 case GLUT_KEY_UP:
      isUpKeyPressed = 0;
 break;

 case GLUT_KEY_DOWN:
      isDownKeyPressed = 0;
 break;

 }
}

void handleKeypress(unsigned char key, int x, int y) {
  switch (key) {
    case 27: //Escape key
         exit(0);
    break;

    case 'F': //Posici�n de la luz
        ++LIGHT_POS;
    break;
    case 'E':
          if (prenderLuz == 1)
             prenderLuz = 0;
         else
            prenderLuz=1;
    break;

    case 'f': //Posici�n de la luz
        --LIGHT_POS;
    break;

    case 'r': //La esfera gira
         if (Rot == 1)
             Rot = 0;
         else
             Rot = 1;
    break;

    case 'l': //Luz
         Luz -= 0.005;
         if (Luz < 0)
             Luz = 1;
    break;

    case 'h':
        if (Zoom > 0)
            Zoom -= 0.01;
    break;

    case 'H':
        if (Zoom < 10)
            Zoom += 0.01;
    break;

    case 't': //Sube
         if (++Transparencia > 255)
             Transparencia = 0;
    break;
    case 'L': //Escape key
         if(miracontrol==0)
         miracontrol=1;
         else
            miracontrol=0;
            break;
     case 'c': //Escape key
         if(trigger==0)
            trigger=1;
         else
            trigger=0;
            break;
    case 'J':cerca += 0.05;break;

    case 'j':
         cerca -= 0.05;
    break;

    case 'm':
         if (Meridianos == 0)
             Meridianos = 1;
         else
             Meridianos = 0;;
    break;

    case 'e':
         if (mTextura == 0)
             mTextura = 1;
         else
             mTextura = 0;
    break;
    case 'p':
        contadorLimite=0;
        planPos=0;
        planVueloIniciado=1;
        permiso=1;
    break;

    case 'A':
        dx += 1;
        break;
    case 'a':
        dx -= 1;
        break;
    case 'B':
        dy += 1;
        break;
    case 'b':
        dy -= 1;
        break;
    case 'Q':
        dz += 1;
        break;
    case 'q':
        dz -= 1;
        break;
  }
}

void init() {
  glClearColor(0, 0, 0, 0); //Fondo negro en toda la escena
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glEnable(GL_NORMALIZE);
  glEnable(GL_COLOR_MATERIAL);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  if(!cargarTGA("C:/Users/geekMQ/Desktop/mesa trabajo/gl/sombrilla/piso.tga", &texturas[0]) ) {
     printf("Error cargando textura\n");
     exit(0); // Cargamos la textura y chequeamos por errores
	}

  if(!cargarTGA("C:/Users/geekMQ/Desktop/mesa trabajo/gl/sombrilla/goku.tga", &texturas[1]) ) {
     printf("Error cargando textura\n");
     exit(0); // Cargamos la textura y chequeamos por errores
	}

  if(!cargarTGA("C:/Users/geekMQ/Desktop/mesa trabajo/gl/sombrilla/goku.tga", &texturas[1]) ) {
     printf("Error cargando textura\n");
     exit(0); // Cargamos la textura y chequeamos por errores
	}



  if(!cargarTGA("C:/Users/geekMQ/Desktop/mesa trabajo/gl/sombrilla/Bola.tga", &texturas[3])){
     printf("Error cargando textura\n");
     exit(0); // Cargamos la textura y chequeamos por errores
	}

	if(!cargarTGA("C:/Users/geekMQ/Desktop/mesa trabajo/gl/sombrilla/Star.tga", &texturas[4])){
     printf("Error cargando textura\n");
     exit(0); // Cargamos la textura y chequeamos por errores
	}
	FILE *myfile;

   if((myfile=fopen("C:/Users/geekMQ/Desktop/mesa trabajo/gl/sombrilla/planVuelo.txt","r") ) == NULL){
     printf("Error cargando plan vuelo\n");
     exit(0); // Cargamos la textura y chequeamos por errores
	}
	k=0;

	while(fscanf(myfile,"%c %f",&Direccion[k],&Desplazamiento[k]) != EOF){
	k++;
	}

	maxPunt=k-1;

	fclose(myfile);
	/*char n=' ';
    for(int i=0 ; i< maxPunt; i++){
      n=Direccion[i];
     printf("%c\n", n);

    }*/


}


void handleResize(int w, int h) {
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, (float)w / (float)h, 1.0, 200.0);
}

void moverHelices(int flag){
    if(flag == 1){
       RotacionHelice += 5;
       if (RotacionHelice >= 360) {
          RotacionHelice = 0;
       }
      }
}

void moverVerticalMente(int val){

          if(val > 0){
               moverVertical=moverVertical-1;

               if(reversaVertical > 0){
                    moverEnY +=incremento;
                 }else{
                       moverEnY -= incremento;
               }

          }
}

void moverLadoZ(int val){

      if(val > 0){
         moverLadoz=moverLadoz-1;

         if(reversaZ> 0){
           moverEnZ +=incremento;
         } else
           moverEnZ -=incremento;
      }
}

void moverLadoXF(int val){

      if(val > 0){
         moverLadoX=moverLadoX-1;
       if(reversaX> 0){
         moverEnX +=incremento;
        }else
         moverEnX -=incremento;
      }

}

void encenderLaser(int val){

        if(!miracontrol==0)
         miracontrol=1;
         else
            miracontrol=0;

}

void Disparar(int val){

         if(ContadorBalas > 0){
            trigger=1;
            ContadorBalas-=1;
         }
         else{
            trigger=0;
         }
}

void spinDisplay(void) {


   Rotacion += 1;
      if (Rotacion >= 360) {
          Rotacion = 0;
   }


if(Inclinacion > 0){
    Inclinacion-=0.01;
   }
   else {
   Inclinacion=30;}

     // Movimiento de translaci�n sobre el piso en espiral
   Angulo += 0.002;
   if (Angulo > 360) {
       Angulo -= 360;
   }

   //Espiral de Bernoulli (logaritmica)
   r = 50 - pow (2.7182, 0.1 * Angulo);     //Debe ajustar a su escena
   if (0 < r) {
       x2 = r * cos (Angulo);
       z2 = r * sin (Angulo);
       //printf("r = %6.4f ang = %5.2f x=%5.2f z=%5.2f\n", r, Angulo, x, z);
   }

      if (rotate <= 30) {
           rotate += 0.01;
           Rotacion2 += 0.40;

           if(contador<limite && limite!=0){
            saltoBola+=0.05;
           }

           if(contador>=limite && limite!=0){
            saltoBola-=0.05;

           }
           if(saltoBola<-21){
                saltoBola=-20;
                if(limite>0){

                    limite-=25;
                }
           contador=0;
           }
        contador++;
       }

/**********************************************************************************************************************************/

    moverHelices(moverHelice);
    moverVerticalMente(moverVertical);
    moverLadoZ(moverLadoz);
    moverLadoXF(moverLadoX);
    encenderLaser(miracontrol);
    Disparar(ContadorBalas);

if(planVueloIniciado==1){
    if(permiso==1){
        aux=0;
        aux=Desplazamiento[planPos];

        switch(Direccion[planPos]){
            case 'E':moverHelice= aux;break;
            case 'Y':reversaVertical =aux;  moverVertical=(aux > 0 ? aux*aumento: aux*aumento*-1);break;
            case 'Z':reversaZ        =aux;  moverLadoz   =(aux > 0 ? aux*aumento: aux*aumento*-1);break;
            case 'X':reversaX        =aux;  moverLadoX   =(aux > 0 ? aux*aumento: aux*aumento*-1);break;
            case 'L':miracontrol=Desplazamiento[planPos];  break;
            case 'B':ContadorBalas=Desplazamiento[planPos];break;
            case 'M':break;
        }
    if(aux<0)
            aux=aux*-1;

        limiteCambioSeleccion=aux*aumento;
        permiso=0;
        contadorLimite=0;
    }
    contadorLimite+=1;
    //printf("contador  %d\n", ContadorBalas);

    if(contadorLimite>= limiteCambioSeleccion){
       if(planPos<= maxPunt){
        contadorLimite=0;
        planPos++;
        permiso=1;
        }
    }
}
   glutPostRedisplay(); //Vuelve a dibujar
}

void mouse(int button, int state, int x, int y) {
   switch (button) {
      case GLUT_LEFT_BUTTON:
         if (state == GLUT_DOWN) {
            glutIdleFunc(spinDisplay);
         }
         break;

      case GLUT_RIGHT_BUTTON:
      case GLUT_MIDDLE_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(NULL);
         break;

      default:
         break;
   }
}
void mouseMotion(int x, int y) {
     spinX = y;
     spinY = x;
     //printf("X %5.2f Y %5.2f\n", spinX, spinY);
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
  glutInitWindowSize(600, 600);

  glutCreateWindow("Sombrilla con reflejo");
  init();
  glutDisplayFunc(drawScene);
  glutKeyboardFunc(handleKeypress);
  glutReshapeFunc(handleResize);
  glutMouseFunc(mouse); //Activa los controles del mouse
  glutMotionFunc(mouseMotion);

    glutSpecialFunc(handleSpecialKeypress);
  glutSpecialUpFunc(handleSpecialKeyReleased);
  glutMainLoop();

  return 0;
}
