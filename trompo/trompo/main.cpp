#include <windows.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
GLint spinX=749, spinY=539; //Para rotar la escena
GLfloat Rotacion = 0;
GLfloat Inclinacion = 30;
GLfloat zoom=0.3;
GLfloat x=2;
GLfloat y=76;
GLfloat z=76;
GLfloat TamaPiso=3;
/*meter en spin displaycodigo de vernulli*/
    GLfloat x2 = 0;       //Posici�n en x
    GLfloat y2 = -7.6;   //Posici�n en y ---- Debe ajustar a su escena
    GLfloat z2 = 0;       //Posici�n en z
    GLfloat Angulo = 0;
    GLfloat r = 0;


void piso(){
glPushMatrix();
glTranslatef(-280,0,-220);
GLfloat Largo=40;
GLfloat Ancho=40;
GLfloat flag=0;
GLfloat mover_derecha=0;
GLfloat mover_abajo=0;
glColor4ub (0,112,0,0);
for (GLint x = 0; x < 8 ;x += 1){
glTranslatef(0,0,mover_abajo);
        for (GLint y = 0; y < 8 ;y += 1){

                   if ( (y %2)== flag){
                        glColor3f(.6, .6, .6);
                    }
                    else{
                        glColor3f(.2, .2, .2);
                    }

                glPushMatrix();
                glTranslatef(mover_derecha,0,0);
                glBegin(GL_QUADS);
                glVertex3f(-Largo,-Ancho,-Largo);
                glVertex3f( Largo,-Ancho,-Largo);
                glVertex3f( Largo,-Ancho, Largo);
                glVertex3f(-Largo,-Ancho, Largo);
                glEnd();
                glPopMatrix();
                mover_derecha+=80;
            }
if(flag ==0)
    flag=1;
else
    flag=0;
    mover_derecha=0;
    mover_abajo=80;

}
    glPopMatrix();
}


void DibujaEsfera (void) {
glTranslatef(0,60,0);
glRotatef(Rotacion, 0, -1, 0);
glRotatef(Inclinacion,1,0,0);
glTranslatef(x2,y2,z2);
    GLUquadricObj * Objeto;
    GLUquadricObj * Objeto1;
    GLUquadricObj * Objeto2;
    GLUquadricObj * Objeto3;
    GLUquadricObj * Objeto4;
    GLUquadricObj * Objeto5;
    GLUquadricObj * Objeto6;
    GLUquadricObj * Objeto7;
    GLUquadricObj * Objeto8;
    GLUquadricObj * Objeto9;
    GLUquadricObj* quadratic;


  glPushMatrix();
    Objeto = gluNewQuadric();
    gluQuadricDrawStyle(Objeto,GLU_FILL);
    glTranslatef(0,0,0);
    glColor4ub(255, 255, 0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto,8,8,5,20,1);
  glPopMatrix();

   glPushMatrix();
    quadratic = gluNewQuadric();
    glTranslatef(0,5,0);
    glRotatef(90,-1,0,0);
    glColor4ub(255, 0, 0,0);
    gluDisk(quadratic,0,9,10,32);
  glPopMatrix();

//tapa
 glPushMatrix();
    quadratic = gluNewQuadric();
    glTranslatef(0,0,0);
    glRotatef(90,-1,0,0);
    glColor4ub(255, 0, 0,0);
    gluDisk(quadratic,0,25,3,32);
  glPopMatrix();


 glPushMatrix();
    Objeto1 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto1,GLU_FILL);
     glColor4ub(255, 255, 0,0);
    glTranslatef(0,-5,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto1,32,25,5,20,1);
 glPopMatrix();

  glPushMatrix();
    Objeto2 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto2,GLU_FILL);
    glTranslatef(0,-10,0);
     glColor4ub(255, 0, 0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto2,34,32,5,20,1);
 glPopMatrix();

   glPushMatrix();
    Objeto3 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto3,GLU_FILL);
    glTranslatef(0,-15,0);
     glColor4ub(255, 255, 0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto3,36,34,5,20,1);
 glPopMatrix();

   glPushMatrix();
    Objeto4 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto4,GLU_FILL);
    glTranslatef(0,-20,0);
     glColor4ub(255, 0, 0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto4,36,36,5,20,1);
  glPopMatrix();

   glPushMatrix();
    Objeto5 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto5,GLU_FILL);
    glTranslatef(0,-25,0);
     glColor4ub(255, 255, 0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto5,34,36,5,20,1);
  glPopMatrix();

glPushMatrix();
    Objeto6 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto6,GLU_FILL);
    glTranslatef(0,-30,0);
     glColor4ub(255, 0, 0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto6,32,34,5,20,1);
  glPopMatrix();

  glPushMatrix();
    Objeto7 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto7,GLU_FILL);
    glTranslatef(0,-75,0);
    glColor4ub(255, 255, 0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto7,0,32,45,20,1);
  glPopMatrix();

glColor4ub(42, 51, 49,0);
glPushMatrix();
    Objeto8 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto8,GLU_FILL);
    glTranslatef(0,-78,0);
    glRotatef(90,-1,0,0);
    gluCylinder(Objeto8,1,1,5,20,1);
  glPopMatrix();

  glPushMatrix();
    Objeto9 = gluNewQuadric();
    gluQuadricDrawStyle(Objeto9,GLU_FILL);
    glTranslatef(0,-80,0);

    glRotatef(90,-1,0,0);
    gluCylinder(Objeto9,0,2,5,20,1);
  glPopMatrix();

}

void DibujaCubo (void) {
     glColor4ub(0, 255, 0, 0);   //Verde
     glutWireCube (100.0);
}

void display(void)
{

   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();

   glRotatef(spinX, 1, 0, 0);
   glRotatef(spinY, 0, 1, 0);
   glScalef(zoom,zoom,zoom);
   piso();
   DibujaEsfera();
   glutSwapBuffers();
}

void spinDisplay(void)
{
   Rotacion +=0.1;
   if(Inclinacion > 0){
    Inclinacion-=0.001;
   }
   else {
   Inclinacion=30;}

     // Movimiento de translaci�n sobre el piso en espiral
   Angulo += 0.002;
   if (Angulo > 360) {
       Angulo -= 360;
   }

   //Espiral de Bernoulli (logaritmica)
   r = 10 - pow (2.7182, 0.1 * Angulo);     //Debe ajustar a su escena
   if (0 < r) {
       x2 = r * cos (Angulo);
       z2 = r * sin (Angulo);
       //printf("r = %6.4f ang = %5.2f x=%5.2f z=%5.2f\n", r, Angulo, x, z);
   }

   glutPostRedisplay(); //Vuelve a dibujar
}

void init(void) {
  glClearColor (0.0, 0.0, 0.0, 0.0); //Colores para iniciar la ventana (Fondo)
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glEnable(GL_NORMALIZE);
  glEnable(GL_COLOR_MATERIAL);
}

void reshape(int w, int h) {
   glViewport (0, 0, (GLsizei) w, (GLsizei) h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();

   glOrtho(-100.0, 100.0, -100.0, 100.0, -100.0, 100.0); //Izq, Der, Abajo, Arriba, Cerca, lejos
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   gluPerspective(45.0, (float)w / (float)h, 100.0, 100.0);
}

void handleKeypress(unsigned char key, int x, int y) {
  switch (key) {
    case 27: //Escape key
         exit(0);
    break;
    case 'z': //Escape key
         zoom -=0.1;
    break;
      case 'Z': //Escape key
         zoom +=0.1;
    break;
     case 'o': //Escape key
         spinX =90;
         spinY =0;
    break;
      case 'O': //Escape key
         spinX =0;
         spinY =90;
    break;
  }
}

void mouseMotion(int x, int y)
{
     spinX = y;
     spinY = x;

    printf("y %d \n", spinY);
    printf("x %d \n", spinX);
}

void mouse(int button, int state, int x, int y)
{
   switch (button) {
      case GLUT_LEFT_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(spinDisplay);
         break;

      case GLUT_RIGHT_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(NULL);
         break;

      default:
         break;
   }
}

int main(int argc, char** argv)
{
   glutInit(&argc, argv);   //Inicializa la librer�a GLUT y negocia una sesi�n
                            //con el sistema de ventanas
   glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH); //Activa el modo inicial de despliegue
                            //GLUT_DOUBLE doble buffer  GLUT_RGB = GLUT_RGBA
   glutInitWindowSize (800, 800); //Tama�o de la ventana 500 x 500
   glutInitWindowPosition (100, 100); //Posici�n de la ventana en la pantalla
   glutCreateWindow ("Esfera - Cubo"); //Crea la ventana y le pone la etiqueta

   init ();  //Ejecuta el m�todo "init"

   glutDisplayFunc(display);
   glutIdleFunc(spinDisplay);
   glutKeyboardFunc(handleKeypress);
   glutReshapeFunc(reshape); //Ejecuta el m�todo "reshape"
   glutMouseFunc(mouse); //Activa los controles del mouse
   glutMotionFunc(mouseMotion);
   glutMainLoop(); //Repite el main indefinidamente (�Animaci�n!)
   return 0;   /* ANSI C requires main to return int. */
}
