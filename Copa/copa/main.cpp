#include <windows.h>
#include <GL/glut.h>

GLint spinX, spinY; //Para rotar la escena
GLfloat Rotacion = 0;


void mostrarEjes(){
//dibujar el eje y
        glBegin(GL_LINES);
        glColor4ub(203, 244, 66,0);
        glVertex3f(0,-1000,0);glVertex3f(0,1000,0);
//x
        glEnd();
        glBegin(GL_LINES);
        glColor4ub(255,0, 0,0);
        glVertex3f(-1000,0,0);glVertex3f(1000,0,0);
        glEnd();
        //z
        glBegin(GL_LINES);
        glColor4ub(32, 178, 58,0);
        glVertex3f(0,0,-1000);glVertex3f(0,0,1000);
        glEnd();

}

void DibujaEsfera (void) {
glTranslatef(0,60,0);
glColor4ub(124, 124, 124,0);
    GLUquadricObj * quadriqCopa;
    GLUquadricObj * quadriqCopa1;
    GLUquadricObj * quadriqCopa2;
    GLUquadricObj * quadriqCopa3;

 glPushMatrix();
    quadriqCopa = gluNewQuadric();
    gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
    //glTranslatef(0,0,0);
    glRotatef(90,-1,0,0);
    gluCylinder(quadriqCopa,30,20,20,20,1);
 glPopMatrix();

 glPushMatrix();
    quadriqCopa = gluNewQuadric();
    gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
    glTranslatef(0,-3,0);
    glRotatef(90,-1,0,0);
    gluCylinder(quadriqCopa,31,29,5,20,1);
 glPopMatrix();

 glPushMatrix();
    quadriqCopa = gluNewQuadric();
    gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
    glTranslatef(0,-5,0);
    glRotatef(90,-1,0,0);
    gluCylinder(quadriqCopa,32,30,5,20,1);
 glPopMatrix();

  glPushMatrix();
    quadriqCopa = gluNewQuadric();
    gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
    glTranslatef(0,-10,0);
    glRotatef(90,-1,0,0);
    gluCylinder(quadriqCopa,34,32,5,20,1);
 glPopMatrix();

   glPushMatrix();
    quadriqCopa = gluNewQuadric();
    gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
    glTranslatef(0,-15,0);
    glRotatef(90,-1,0,0);
    gluCylinder(quadriqCopa,36,34,5,20,1);
 glPopMatrix();

   glPushMatrix();
    quadriqCopa = gluNewQuadric();
    gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
    glTranslatef(0,-20,0);
    glRotatef(90,-1,0,0);
    gluCylinder(quadriqCopa,36,36,5,20,1);
  glPopMatrix();

   glPushMatrix();
    quadriqCopa = gluNewQuadric();
    gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
    glTranslatef(0,-25,0);
    glRotatef(90,-1,0,0);
    gluCylinder(quadriqCopa,34,36,5,20,1);
  glPopMatrix();

glPushMatrix();
    quadriqCopa = gluNewQuadric();
    gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
    glTranslatef(0,-30,0);
    glRotatef(90,-1,0,0);
    gluCylinder(quadriqCopa,32,34,5,20,1);
  glPopMatrix();

  glPushMatrix();
    quadriqCopa = gluNewQuadric();
    gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
    glTranslatef(0,-75,0);
    glRotatef(90,-1,0,0);
    gluCylinder(quadriqCopa,5,32,45,20,1);
  glPopMatrix();

  /*base de la copa*/

       //base superior helice
  glPushMatrix();
    quadriqCopa = gluNewQuadric();
    gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
    glTranslatef(0,-100,0);
    glRotatef(90,-1,0,0);
    gluCylinder(quadriqCopa,5,5,30,10,1);
  glPopMatrix();

  glPushMatrix();
    quadriqCopa = gluNewQuadric();
    gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
    glTranslatef(0,-110,0);
    glRotatef(90,-1,0,0);
    gluCylinder(quadriqCopa,20,5,10,10,1);
  glPopMatrix();

  glPushMatrix();
    quadriqCopa = gluNewQuadric();
    gluQuadricDrawStyle(quadriqCopa,GLU_FILL);
    glTranslatef(0,-110,0);
    glRotatef(90,-1,0,0);
    gluCylinder(quadriqCopa,20,0,10,10,1);
  glPopMatrix();
    gluDeleteQuadric(quadriqCopa);
}

void DibujaCubo (void) {
     glColor4ub(0, 255, 0, 0);   //Verde
     glutWireCube (100.0);
}

void display(void)
{

   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();

   //Rota la escena
   glRotatef(spinX, 1, 0, 0);
   glRotatef(spinY, 0, 1, 0);

   DibujaEsfera();
    mostrarEjes();

   glutSwapBuffers();
}

void spinDisplay(void)
{
   Rotacion += 0.01;
   glutPostRedisplay(); //Vuelve a dibujar
}

void init(void) {
  glClearColor (0.0, 0.0, 0.0, 0.0); //Colores para iniciar la ventana (Fondo)
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glEnable(GL_NORMALIZE);
  glEnable(GL_COLOR_MATERIAL);
}

void reshape(int w, int h) {
   glViewport (0, 0, (GLsizei) w, (GLsizei) h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();

   glOrtho(-100.0, 100.0, -100.0, 100.0, -100.0, 100.0); //Izq, Der, Abajo, Arriba, Cerca, lejos
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   gluPerspective(45.0, (float)w / (float)h, 100.0, 100.0);
}

void handleKeypress(unsigned char key, int x, int y) {
  switch (key) {
    case 27: //Escape key
         exit(0);
    break;
  }
}

void mouseMotion(int x, int y)
{
     spinX = y;
     spinY = x;
}

void mouse(int button, int state, int x, int y)
{
   switch (button) {
      case GLUT_LEFT_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(spinDisplay);
         break;

      case GLUT_RIGHT_BUTTON:
         if (state == GLUT_DOWN)
            glutIdleFunc(NULL);
         break;

      default:
         break;
   }
}

int main(int argc, char** argv)
{
   glutInit(&argc, argv);   //Inicializa la librer�a GLUT y negocia una sesi�n
                            //con el sistema de ventanas
   glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH); //Activa el modo inicial de despliegue
                            //GLUT_DOUBLE doble buffer  GLUT_RGB = GLUT_RGBA
   glutInitWindowSize (600, 600); //Tama�o de la ventana 500 x 500
   glutInitWindowPosition (100, 100); //Posici�n de la ventana en la pantalla
   glutCreateWindow ("Esfera - Cubo"); //Crea la ventana y le pone la etiqueta

   init ();  //Ejecuta el m�todo "init"

   glutDisplayFunc(display);  //Ejecuta el m�todo "display"
   glutKeyboardFunc(handleKeypress);
   glutReshapeFunc(reshape); //Ejecuta el m�todo "reshape"
   glutMouseFunc(mouse); //Activa los controles del mouse
   glutMotionFunc(mouseMotion);
   glutMainLoop(); //Repite el main indefinidamente (�Animaci�n!)
   return 0;   /* ANSI C requires main to return int. */
}
