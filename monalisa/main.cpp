/*
 *  Ejemplo 1: B�sico con textura
 */
#include <windows.h>

#include <GL/glut.h>
#include "TGATextura.h" //Este include administra los archivos TGA

//Aqu� se hace el dibujo
void display(void) {
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D,texturas[0].ID);

  //glColor4ub(112, 112, 112, 0); //El color afecta la imagen
  glBegin(GL_QUADS);
    glTexCoord2f(0, 0); glVertex2f(-1, -1);  // Bottom Left Of The Texture and Quad
    glTexCoord2f(1, 0); glVertex2f( 1, -1);  // Bottom Right Of The Texture and Quad
    glTexCoord2f(1, 1); glVertex2f( 1,  1);  // Top Right Of The Texture and Quad
    glTexCoord2f(0, 1); glVertex2f(-1,  1);  // Top Left Of The Texture and Quad
  glEnd();
  glDisable(GL_TEXTURE_2D);
  glutSwapBuffers();
}

void init(void)
{
  glClearColor (0, 0, 0, 0); //Colores para iniciar la ventana (Fondo)

  if(!cargarTGA("C:/Users/geekmq/Desktop/mesa trabajo/open gl/GL/monalisa/piso.tga", &texturas[0]) ) {

     printf("Error cargando textura\n");
     exit(0); // Cargamos la textura y chequeamos por errores
	}
}

int main(int argc, char** argv)
{
   glutInit(&argc, argv);   //Inicializa la librer�a GLUT y negocia una sesi�n
                            //con el sistema de ventanas
   glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB); //Activa el modo inicial de despliegue
                            //GLUT_DOUBLE doble buffer (Animaciones)  GLUT_RGB = GLUT_RGBA
   glutInitWindowSize (500, 500); //Tama�o de la ventana 500 x 500
   glutInitWindowPosition (100, 100); //Posici�n de la ventana en la pantalla
   glutCreateWindow ("Mona Lisa"); //Crea la ventana y le pone la etiqueta
   init ();  //Ejecuta el m�todo "init"
   glutDisplayFunc(display);  //Ejecuta el m�todo "display"
   glutMainLoop(); //Repite el main indefinidamente (�Animaci�n!)
   return 0;   /* ANSI C requires main to return int. */
}
